import { resolve } from 'path'
import { fileURLToPath } from 'url'
import defu from 'defu'
import { defineNuxtModule, addPlugin, extendPages, addComponent } from '@nuxt/kit'

export interface ModuleOptions {
  appId?: string
  addPlugin: boolean
}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: 'nuxt-git-cms',
    configKey: 'nuxtgitcms'
  },
  defaults: {
    addPlugin: true
  },
  setup (options, nuxt) {
    nuxt.options.runtimeConfig.public.nuxtgitcms = defu(
      nuxt.options.runtimeConfig.public.nuxtgitcms,
      {
        appId: options.appId,
        addPlugin: options.addPlugin
      }
    )

    const runtimeDir = fileURLToPath(new URL('./runtime', import.meta.url))

    if (options.addPlugin) {
      nuxt.options.build.transpile.push(runtimeDir)
      addPlugin(resolve(runtimeDir, 'git-connect.client'))
    }

    extendPages((pages) => {
      // Add /test page
      pages.push({
        name: 'Admin',
        path: '/admin',
        file: resolve(__dirname, './pages/admin.vue')
      },
      {
        name: 'Login',
        path: '/admin/login',
        file: resolve(__dirname, './pages/login.vue')
      })
    })

    const components = [
      'RepositoryTree', 'TreeItem', 'ContentEditor', 'MilkdownEditor'
    ]
    for (const name of components) {
      addComponent({name, global:true, filePath: resolve(__dirname, `./components/${name}.vue`)})
    }
  }
})
