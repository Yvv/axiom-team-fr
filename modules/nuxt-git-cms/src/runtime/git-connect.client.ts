import { defineNuxtPlugin, useRuntimeConfig } from '#app'
import { createClient, GitConnectClient } from 'git-connect'

declare module '#app' {
  interface NuxtApp {
    $git: GitConnectClient
  }
}

export default defineNuxtPlugin(() => {
  const config = useRuntimeConfig()

  // TODO: check if appID is here ? TS do that ?
  const GitConnectClient = createClient({
    appId: config.nuxtgitcms.appId,
    redirectPath: '/admin/login',
  })

  return {
    provide: {
      git: GitConnectClient
    }
  }
})
