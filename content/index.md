---
title: Accueil
---

<div class="bg-gray-600">
Une campagne de financement participatif est en cours sur notre HelloAsso jusqu'au 31 Décembre 2022
<img src="uploads/gpotfresque.svg" style="width: 400px; margin: auto;">

[Voir la campagne][0]
</div>

# Au service de la monnaie libre
:image{src="uploads/LOGOGG1generic_blanc.png" width="400"}

Nous croyons en la monnaie libre et souhaitons faciliter sa promotion ainsi que le travail de ses développeurs.
  
[Statuts de l'association][1] 
[Adhérer][2]

## Mais c'est quoi la monnaie libre ?

Si vous souhaitez d'abord en apprendre plus sur la monnaie libre, rendez-vous sur le site [**https://monnaie-libre.fr**][3]

## L'association _Axiom-Team_ remplit deux missions :

**Aider les membres à promouvoir la monnaie libre** en fournissant aux utilisateurs des outils pour organiser des événements locaux. Nous proposons des créations graphiques (modèles de flyers, d'affiches, de stickers) et des conseils logistiques sous forme de documents 
[disponibles en ligne][4].

**Soutenir les développeurs de l'écosystème Duniter** en mettant à disposition des outils d'organisation et des financements. Pour cela, nous avons 
[deux cagnottes][4] permanentes destinées à accueillir les dons en euro et ğ1.

## Lauréat de l'innovation open source !

Axiom-Team obtient en 2019 le prix de l'innovation open source par [https://lesacteursdulibre.com/][5] pour l'innovation éthique et écologique du projet de monnaie libre Ḡ1, et pour son soutien à l'équipe de développeurs du logiciel Duniter et de son écosystème.

[Lire l'article][6]

:image{src="/uploads/Sans%20titre.png" width="400"}
:image{src="/uploads/gecko_final.png" width="400"}

::FeaturedArticles{}
::

## L'ADEME finance le projet Ğecko !

L'[ADEME][7] a lancé en mars 2021 un appel à projet sur le thème des communs. Son objectif est de favoriser la résilience des territoires via des projets collectifs ayant un impact à l'échelle locale.

Parmi 247 communs recensés sur [le wiki][8], Ğecko a été sélectionné pour financement.

Plus d'informations sur le blog du site duniter.fr:

[Lire l'article][9]

## Contribuer financièrement

Vous pouvez contribuer par **un don en June (Ǧ1)** sur le compte d'Axiom Team.
  
Clé publique :
[8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh][10]

Pour une **participation en euros**
 (don, adhésion, cagnotte, etc..) vous pouvez passer par notre **[page d'adhésion][2]**
 , ou en envoyant un chèque à : Axiom Team 14 rue Johannes Kepler 31500 Toulouse.
  
[Faire un don][2]

:image{src="/uploads/tirelire.png" width="400"}

## Des antennes Axiom Team

L'association fonctionne par antennes régionales de manière à décentraliser l'organisation des événements.

Chaque antenne régionale dispose de [la couverture d'assurance que lui fournit l'association Axiom-Team][11].

Elle dispose également du support et de l'entraide du réseau monnaie libre, et l'accompagnement dans les démarches administratives si il y en a.

Pour postuler en tant qu'antenne d'Axiom-Team, il vous suffit d'envoyer un mail à [contact@axiom-team.fr](mailto:contact@axiom-team.fr)
  
Prévoyez 2/3 jours de délais de réponse.

La charte des antennes est en cours de réalisation et sera bien sûr publiée ici-même.

## Carte des événements monnaie libre, ainsi que des membres Ğ1  

[0]: https://www.helloasso.com/associations/axiom-team/collectes/financement-de-la-g1v2
[1]: https://cloud.axiom-team.fr/s/Dwzpw7PAofNroof
[2]: https://axiom-team.fr/adherer
[3]: https://monnaie-libre.fr/
[4]: https://axiom-team.fr/#
[5]: https://lesacteursdulibre.com/
[6]: https://axiom-team.fr/blog/ressources-1/post/la-g1-grande-gagnante-du-prix-de-linnovation-open-source-2019-9
[7]: https://www.ademe.fr/
[8]: https://wiki.resilience-territoire.ademe.fr/wiki/%C4%9Eecko
[9]: https://duniter.fr/blog/financement-ademe/
[10]: https://g1.duniter.fr/#/app/wot/8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh/
[11]: https://www.maif.fr/associationsetcollectivites/associations/solutions-petites-moyennes-associations/accueil.html