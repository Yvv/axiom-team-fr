---
title: "Financement participatif - faites grandir la Ğ1"
description: "Nous lançons le financement participatif de l'écosystème Duniter v2. Faites grandir la Ğ1 !"
thumbnail: "/uploads/divers/g-pot.svg"
date: 2022-09-30
featured: true

authors: ["AxiomTeam",]
category: ["Financement",]
---

# Financement participatif 

<Note type="info"></Note>
</Note>

::alert{type="info"}
Cet article a été publié initialement sur [https://duniter.fr/blog/financement-participatif/](https://duniter.fr/blog/financement-participatif/)
::

Nous [annoncions récemment](./2022-09-15-financement-ademe.md) le lancement prochain d'un financement participatif de l'écosystème Duniter v2. Nous y voilà !

Jusqu’à aujourd’hui, le développement de Duniter a été **entièrement bénévole**, bien qu’encouragé par une caisse de dons en Ǧ1.

:button-link[Caisse de dons en Ğ1]{icon="openmoji:money-bag" href="https://forum.duniter.org/t/caisse-de-dons-pour-les-contributeurs-techniques-a-lecosysteme-g1/6908" blank}

Comme vous l’avez peut-être entendu, nous préparons une version 2 de Duniter qui corrigera tous les bugs connus comme notamment :

- la synchronisation des *piscines*
- le choix du *nœud* Duniter

et fournira un bien meilleur confort d’utilisation

- un bloc toutes les *6 secondes* plutôt que *5 minutes*
- validation des *certifications* dès l’émission
- nouveaux logiciels clients bien plus *rapides* (Cesium v2, Ğecko)

![g-pot](/uploads/divers/g-pot.svg)

> Faites grandir la Ğ1 !

La **ǦDev**, monnaie de test de cette nouvelle version de Duniter, a été lancée en mai 2022 lors des **RML16** hébergées par Philippe Guillemant. Cette seizième rencontre technique a notamment vu l’arrivée de contributeurs espagnols, un des signes que nous sommes prêts à passer à la vitesse supérieure.

📝 [Compte-rendu des RML16](https://forum.monnaie-libre.fr/t/compte-rendu-des-rml-16-a-destination-des-utilisateurs-de-la-monnaie-libre/23118) / 📹 [Reportage vidéo des RML16](https://forum.monnaie-libre.fr/t/reportage-video-des-rml16/23555)

Pour tenir la communauté informée des changements qui se préparent, [@Elois](../equipe/elois.md) a donné une conférence lors de l’université d’été de la monnaie libre près de Toulouse : « Ğ1 v2.0 : Ce qui va changer ».

🎤 [Conférence "Ğ1 v2.0 : Ce qui va changer"](https://forum.monnaie-libre.fr/t/conference-g1-v2-0-ce-qui-va-changer/23642) / [▶️ sur peertube](https://tube.p2p.legal/w/eVrbpbSQB2cuhnYk7Wrhyy)

Nous travaillons sur d’autres supports pour expliquer au mieux le nouvel écosystème, afin de préparer la transition en douceur que nous souhaitons proposer d’ici deux ans.

---

Le travail à accomplir d’ici la migration nécessite un effort conséquent. Les soirées et weekends consacrés bénévolement ne suffiront pas à arriver à terme, en plus d’être un régime fatigant. Heureusement, plusieurs d’entre nous sont prêts à mettre de côté leur emploi rémunéré en € pour y consacrer les précieuses heures de journée.

Pour garantir que ce travail se fasse dans de bonnes conditions, il faut être en mesure de répondre aux besoins primaires des concernés (loyer, charges, alimentation) pendant une durée minimum d’un an.

Ces besoins ne pouvant pour l’instant pas être comblés exclusivement en Ǧ1, nous avons décidé de lancer un appel à don en €. Les membres et utilisateurs de la Ǧ1 qui souhaitent soutenir son développement technique et peuvent se le permettre sont invités à participer à cette campagne.

:button-link[Financement participatif en €]{icon="openmoji:euro-banknote" href="https://www.helloasso.com/associations/axiom-team/collectes/financement-de-la-g1v2" blank}

Cet appel à don initial doit nous donner la liberté nécessaire pour mettre en place une solution pérenne. Nous détaillerons ces solutions en temps voulu.

Le montant collecté sera réparti de manière transparente entre les développeurs suivant la méthode du cercle de répartition (cf 
[Co-responsabilité financière](https://forum.duniter.org/t/co-responsabilite-financiere/9720)).