---
title: Paola
thumbnail: /uploads/avatar/italpaola.png
---

Bonjour à tous (y a t il des toutes :face_with_monocle: )

Italienne, arrivée en france à 20 ans et depuis j’y suis restée. Donc bilingue, traductrice-interprète assermentée français/italien. Prof qui ne travaille pas, en préretraite, j’ai du temps pour me consacrer à mes loisirs et mes passions. Ancienne instructrice d’autodéfense pour femmes, je participe encore aujourd’hui à différents collectifs qui œuvrent pour la liberté, je soutiens et j’aide quand je peux.
Hypersensible, franche, directe, hyperactive, pragmatique, honnête, indépendante, passionnée (et oui,italienne!), polyvalente … j’aime jouer, danser, jardiner, bricoler …j’apporte souvent la contradiction constructive à qui peut l’entendre, mon objectif serait d’avoir la CNV dans tout dialogue …Je n’ai pas ma langue dans ma poche. Je ne supporte pas l’autoritarisme, la manipulation, le faux, les aubergines :wink: …

Mon premier essai pour rentrer dans la G1 date de il y a plus de deux ans, à ce propos je crois qu’il doit y avoir un compte portefeuille qui a du se supprimer tout seul. Dans la toile de confiance depuis un an.
A Béziers nous avons Pascal Guillemain qui oeuvre depuis le début pour la diffusion de la june. Avec ses explications on sait pourquoi on rentre dans la June et comment, il est excellent pour ca. Donc des Gmarchés ont déjà eu lieu chez moi. En septembre nous (groupe jeunistes Béziers) avons décidés de créer un canal TG dont techniquement je suis la propriétaire.

J’ai croisé Python, PHP, que j’ai détesté, (à la fac licence info-com en plus d’autres licences, beaucoup de MOOC) quelques souvenirs de HTML/CSS et je sais monter mon ordi :sweat_smile: qui n’est pas sous Linux car en prime j’aime le confort

Pour la petite histoire : POKA est arrivé sur un groupe TG en disant que c’était pour nous présenter les changements qui allaient arriver dans DUNITER2VS et puisque je suis curieuse, j’aime apprendre, et que chaque fois je peste avec la lenteur de Cesium (j’ai même crée mes carnets papiers G1), même pas peur ! j’ai contacté Poka pour lui proposer mon aide pour des traductions → forum DUNITER → Hugo → équipe CF → scan → forum ML → équipe de com du CF → adhésion Axiom-Team pour soutenir les dev, le CF, la June.

Mes compétences qui peuvent intéresser Axiom-Team : traduction italien, rédaction, correction orthographique, esprit de synthèse, capacité de structuration, mindmapping, je sais remplir des dossiers de demandes en tout genre, je maîtrise TG je n’aime pas FB (vous voulez mon cv ? :sweat_smile:)

Merci pour votre accueil, votre confiance, et si je peux aider, bien contente. Au plaisir