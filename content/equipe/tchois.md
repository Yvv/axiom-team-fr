---
title: François
thumbnail: /uploads/avatar/tchois.png
---

Moi c'est François ou Tchois en nissart (niçois).

Donc je suis de la région niçoise.
J'ai découvert la ML au cours d'une réunion GJ en octobre 2019, méfiant au début (trop beau pour être vrai), mais convaincu après une nuit de recherche :grin:.
Membre depuis février 2020, avec un microgroupe nous avons porté la june localement, puis à force de regrouper du monde avons réussit à organiser nos premiers événements.
Animateur de la visio du mardi soir à 20h30 sur https://meet.jit.si/Monnaie-libre.
Créateur et/ou modo de quelques groupes Telegram.
Depuis peu trésorier de la partie Ğ1 de l'asso Axiom-Team.
Magasinier-cariste en intérim depuis plus de 20ans.
Perso, j'aime la moto (sur Suzuki GSX-F750 depuis 19 ans), le foot (uniquement le club de ma ville), l'informatique (quelques connaissances en html et php), la pizza...