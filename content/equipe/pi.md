---
title: Pi Nguyen
thumbnail: /uploads/avatar/pi.png
---

Bonjour,

Je suis Brad Pi, alias le Capitaine Pi.
J’habite à aux portes de Toulouse sur le Canal du Midi sur la PiNiche Chèvrefeuille.
Je suis dans la Ğ1 depuis Avril 2017, je fais la promotion de la Monnaie Libre au travers de présentations avec une vue utilisateur en tentant de la vulgariser et avec humour, j’organise également des événements pour favoriser les rencontres entre utilisateurs et développeurs.

J’ai eu l’idée de créer l’association Axiom Team dans l’objectif de fédérer les forces autour de moi pour promouvoir la ML là où elle existe et pour soutenir les développeurs.

Officiellement Président de l’association aujourdhui, je souhaite participer au changement de notre société, pour la rendre plus juste et plus égalitaire pour les générations futures.

Pour moi les notions de confiance et de liberté sont très fortes, elle guident ma démarche.
Participer à l’essor d’une monnaie libre c’est participer à la fondation d’une réelle démocratie, je suis convaincu du chemin que nous avons emprunté ensemble, cette expérience est jusqu’ici prometteuse, j’espère que l’humain saura en faire bon usage.

La route est longue, mais la voie est libre.