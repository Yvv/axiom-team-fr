# Site axiom-team.fr

Look at the [nuxt 3 documentation](https://v3.nuxtjs.org) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# pnpm
pnpm install --shamefully-hoist

# yarn
yarn install

# npm
npm install
```

## Development Server

Start the development server on http://localhost:3000

```bash
# pnpm
pnpm dev

# yarn
yarn run dev

# npm
npm run dev
```

## Production

Build the application for production in ssr mode (should be served by node.js) :

```bash
pnpm run build
```

Build the application in static mode :

```bash
pnpm run generate
```
All the pages will be generated at this build time and accessible in `.output/public/`

Locally preview production build:

```bash
pnpm run preview
```

Checkout the [deployment documentation](https://v3.nuxtjs.org/guide/deploy/presets) for more information.
