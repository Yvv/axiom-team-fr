import { execSync } from 'child_process'
import { transformerDirectives } from 'unocss'

import config from './content/settings.json'

const publicRuntimeConfig = {
  site_url: process.env.site_url || '',
  twitter_user: process.env.twitter_user || '',
  social_networks_hashtags: process.env.social_networks_hashtags || '',
  git_commit: execSync('git log --pretty=format:"%h" -n 1').toString().trim(),
  ...config
}

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  modules: [
    // https://content.nuxtjs.org/
    '@nuxt/content',
    // https://v1.image.nuxtjs.org/
    '@nuxt/image-edge',
    // https://github.com/kevinmarrec/nuxt-pwa-module
    '@kevinmarrec/nuxt-pwa',
    // https://uno.antfu.me/
    '@unocss/nuxt'
  ],

  // https://elements.nuxt.space
  extends: '@nuxt-themes/elements',

  // Config
  publicRuntimeConfig,

  /**
   * Global page headers (https://v3.nuxtjs.org/api/configuration/nuxt.config#head)
   */
  app: {
    head: {
      title: config.site_title || process.env.npm_package_name || '',
      meta: [
        {
          name: 'description',
          content:
            config.site_description || process.env.npm_package_description
        }
      ],
      link: [
        {
          rel: 'stylesheet',
          href: 'https://fonts.bunny.net/css?family=changa-one:400,400i"'
        },
        { rel: 'icon', type: 'image/x-icon', href: config.site_logo }
      ],
      bodyAttrs: {
        // For tailwindcss in dev mode
        class: process.env.NODE_ENV === 'development' ? 'debug-screens' : ''
      },
      htmlAttrs: {
        // Force dark mode
        class: 'dark'
      }
    }
  },

  unocss: {
    preflight: true,
    typography: true,
    icons: true,
    transformers: [transformerDirectives({ enforce: 'pre' })] // use enforce: 'pre' to correcty handle @apply. See https://github.com/unocss/unocss/issues/809#issuecomment-1118632177
  },

  images: {
    provider: 'netlify'
  },

  // https://pwa.nuxtjs.org/manifest
  pwa: {
    meta: {
      name: config.site_title,
      author: null,
      lang: 'fr',
      theme_color: '#0d9488',
      description: config.site_description,
      ogHost: config.site_url,
      twitterCard: 'summary_large_image'
    },
    manifest: {
      name: config.site_title,
      short_name: config.site_url,
      lang: 'fr'
    },
    icon: {
      fileName: config.site_logo
    }
  },

  nuxtgitcms: {
    appId: 'c544617371e801310739cce2eb975ab646ad84b53a64d1f69b99b223b1beb361'
  },

  build: {
    transpile: ['@yeger/vue-masonry-wall', 'swiper']
  }
})
